# Conditional Flow Statements and Looping Constructs

1. Rewrite the following for statement as an equivalent while statement.

    ```cpp
    for ( i = 0; i < max_length; ++i )
      if ( input_line[i] == '?' ) ++count;
    ```

2. Write a program that prompts for two arguments and an operator. Using a switch statement, evaluate the expression formed by the operator and operands.

    For example:

    ```console
    Please input two operands:  2  3
    Please input an operator:  *
    Multiplying 2 and 3 = 6

    Please input two operands: 4 8
    Please input an operator: +
    Adding 4 and 8 = 12
    ```

3. The following program fragment reads characters from standard input and writes them to the standard output until end-of-input is detected. Write a program to verify this behaviour. Note that end-of-input on Unix machines is indicated by ^D.

    ```cpp
    char c;
    while (std::cin.get(c))
      std::cout.put(c);
    ```

    `get()` and `put()` are low-level functions for `cin` and `cout`. They do not format the input or output, i.e. spaces and tabs are left as normal characters. The above while loop terminates on `^D` because `std::cin.get()` returns 0 when the end-of-file is read. You can find more information on `cin.get()` [here](http://www.cplusplus.com/reference/istream/istream/get/?kw=cin.get) and on `cout.put()` [here](http://www.cplusplus.com/reference/ostream/ostream/put/?kw=cout.put).

4. Write a program which reads in a line of text, reverses all of the characters, and writes out the reversed line.

    In this exercise use the `cin.get()` function to ensure that the entire line is reversed, not just the first word.

5. Write a program that reads a line of words from the input and prints out the longest word and its length. Make use of standard library types such as
`std::string`, `std::list`, and `std::vector`.

_Optional Question_

6. Write a program tab which replaces strings of blanks by the minimum number of tabs and blanks to achieve the same spacing.

    The program should run until end-of-input is read. Each line read should be written out to the standard output in its updated form.

    Notes

    - `\t` is the tab character and usually corresponds to 8 spaces, or the minimum to align to the next tab-stop

    - Create the input in a separate file and store the output in a separate file, but get Unix to do the file handling. Use the Unix shell to redirect I/O

    ```sh
    tab < input > output
    ```

    - To verify that tabs have indeed been written, use the Unix `od -a` command to gain an ascii dump

    ```sh
    od -a output
    ```

    - Compare this output with that from the Unix command `unexpand -a`.

    - Consider which standard library types and functions will be useful in implementing `tab`.
