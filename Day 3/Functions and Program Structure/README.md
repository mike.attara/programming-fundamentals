# Functions and Program Structure

1. Write a function called `input` that prompts for two integers and then passes these back to the caller. The calling function should simply add them together and output the result.

2. Write a collection of functions to output values of different types onto the display. All of the functions should have the same name, but be overloaded for `int`, `char`, `float`, `long` and `std::string`. The functions should also output their type.

    Write a program to test these functions. Exercise the functions using both literal constants and variables.

    ```cpp
    output(3.14)
    output("hi there");
    output(myname);
    ```

    Which function(s) does the call to `output(3.14)` attempt to bind to and why?

3. Replace the `std::string` overload above with one that takes a `char []`. Overload the function to optionally take two or three parameters.

    ```cpp
    output(myname)
    // causes the complete string to be displayed on the terminal

    output(myname,3)
    // causes part of the string to be displayed starting from character 3

    output(myname,3,4)
    // causes a sub-string of myname to be displayed starting at offset 3 and of length 4.
    // (A length of -1 may be used to indicate end of string).
    ```

4. Rewrite the overloaded `char []` versions of output above so that the same behaviour is achieved with only one function.

5. Break up the program developed in questions 2, 3 and 4 so that the functions exist in a separate compilation unit (library) than the application program (main) that is using them. Setup a header file to properly declare and prototype the functions.

    Create object files for the main and library files, and examine the symbol tables of each using the `nm` command. Specifically identify unresolved symbols and symbol definitions. Use the linker (`g++ a.o b.o`) to create an executable and test.

6. Create a function `factorial` that takes a single integer argument and returns its factorial. Provide a recursive implementation.
