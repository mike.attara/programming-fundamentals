# Operators & Expressions

1. Rewrite the following expression without the use of `&&` and not by using conditional statements like `if`.

    ```cpp
    ((i<limit-1 && (c=getchar()) != '\n') && c != 'A')
    ```

    **Hint**: Use !DeMorgan's theorem which states:

    - not (**A** and **B**) is the same as (not **A**) or (not **B**)

    - not (**A** or **B**) is the same as (not **A**) and (not **B**)
  
2. If `x` is `1` and `y` is `2` then `x` & `y` is `zero` while `x` && `y` is `1`. Why?

3. Write and test the function rotate. The function receives two integer arguments, the value to be rotated (`s`) and a count of the number of times to rotate (`r`). Having rotated the value in `s` it provides this result as its return value.

    ```cpp
    unsigned int rotate (unsigned int s, unsigned int r)
    ```

    The function must rotate s to the left r times, shifting bits from the right (LSB) to the left (MSB). The test program should prompt for values from the user, and call rotate. The program should then output both the original value and the shifted value in **hexadecimal**.

    ```cpp
    input two values: 4 2
    original: 4
    rotated:  10
    ```

    **Note**: a `hex` manipulator is available for `std::cout` which displays the following integer in hexadecimal.

    ```cpp
    std::cout << std::hex << 22;
    ```

    **NB**: You should not use a loop in your implementation of `rotate`. You should also ensure that your implementation is portable.

4. Using a single `while` statement (with an empty body) and comma operators, write a program that displays 10 powers of 2.

## Optional Questions

1. Extend the naughts-and-crosses program to check for the end of the game (i.e., when any row, column or diagonal is filled with either `X` or `O`) and report the result.

2. Extend the naughts-and-crosses game so that a single user plays against the computer! You might like to develop this program gradually as the course progresses, including refinements as they occur to you.
