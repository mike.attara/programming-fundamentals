# Intrinsic Data Types

1. Given the following enumeration type, write a program to output the values associated with each symbol.

    ```cpp
    enum {RED, YELLOW, AMBER=YELLOW, GREEN};
    ```

2. Write a program in which you define variables of the following types

    - integer
    - pointer to integer
    - reference to integer
    - constant integer

    Perform the following actions on the variables

    - assign the value 5 to the integer
    - increment the integer through the pointer
    - increment the integer through the reference
    - output the integer and verify that its value is 7

    What happens if you attempt to change the value of the constant?

    Output the decimal values of the following literals:

    - 0xf3f2
    - 0437
    - 'a'

3. Write a program which reads a single line of hyphen connected words and outputs the words one per line, with a count of the number of characters in each. Your solution should employ primitive C style strings, using character arrays and pointers.

    ```console
    Enter word: hello-this-is-a-line-of-input
    [5] hello
    [4] this
    [2] is
    [1] a
    [4] line
    [2] of
    [5] input
    ```

    Note: when using the input operator `>>` all characters are read until a white-space character is found.

    Ensure that your solution to this and all subsequent questions is properly indented and formatted.

4. Using the C++ Standard Library string, create a type (using `typedef`) for an array of 10 strings. Using the type, create an array and then read 10 string objects from the standard input, and output the string with the largest size.

5. Repeat the previous exercise using a type alias. Which syntax do you find more readable?

## Optional Questions

1. Write a 2 dimensional noughts and crosses game (tic-tac-toe), implemented with either a 1D or 2D array. The game should be displayed using an `X` character for a cross, `O` for a nought, and `?` for free location.

    The program should first display an empty board (full of `?`s), and repeatedly prompt two users for their next move. Requests for `X`s and `O`s are made alternately. Moves may be indicated by a two digit code (`0 0` - top left, `2 2` bottom right), or any other mechanism considered helpful. At the start of the game, and after each move the board should be re-displayed.

    ```console
    ?  ?  ?
    ?  ?  ?
    ?  ?  ?

    X move: 1 1
    ?  ?  ?
    ?  X  ?
    ?  ?  ?

    O move:
    ```

    Note that this exercise will be extended in later chapters.
