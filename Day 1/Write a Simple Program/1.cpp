#include <iostream>

/**
 * main - display my name
 * Return: 0
 */
int main(void)
{
  std::cout << "Mike Attara" << std::endl;

  return (0);
}