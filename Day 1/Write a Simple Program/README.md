# Writing a Simple Program

1. Write a C++ program to display your name. The program should use the iostream class library

2. Write a program which requests 5 integers from the user and then displays the mean (and if you have the appropriate knowledge - also the mode and median) of the values entered.

   (Note, `/` is used for division)

3. Write a function which prompts for a number, and returns this to the main program. If the number is even, produce a twenty row multiplication table for the number. If the entered value is odd, then produce a table containing the first thirty integers NOT divisible by the entered value.

   (Note, `%` produces the modulus, or remainder, for integer division).

    ```console
    Please enter a number: 3
    1
    2
    4
    5
    7
    8
    10
    11
    13
    ...

    Please enter a number: 2
    1 x 2 = 2
    2 x 2 = 4
    3 x 2 = 6
    4 x 2 = 8
    5 x 2 = 10
    ...
    ```

## IO Stream Examples

```cpp
# include <iostream>

int main()
{
// reading on type
int i;
float f;
char ca [100];
std::string s;
std::cin >> i >> f >> ca >> s;
std::cout << i << f << ca << s;
}
```

```cpp
# include <iostream>

int main()
{
   // reading an int with error checking
   int i;
   while (std::cin >> i,  !std::cin) {
      std::cin.clear();
      std::cin.ignore (256, '\n');
   }
   std::cout << i;
}

// to be more precise, clear stream up to limit of how much could be there or end of line
// std::cin.ignore (std::numeric_limits<std::streamsize>::max(), '\n');
```

```cpp
# include <iostream>

# include <iomanip>

int main()
{
   char buffer[256];

   // read white-space delimted word
   std::cin >> buffer;

   // as above with buffer overflow check
   std::cin >> std::setw(256) >> buffer;

   // reads until buffer full or delimiter found or end-of-file
   std::cin.getline (buffer, sizeof(buffer));

   // as above but specify delimiter - reads & discards delimiter
   std::cin.getline (buffer, sizeof(buffer), '.');

   // as above - leaves delimiter in stream
   std::cin.get (buffer, sizeof(buffer), '.');

   // read & parse by character, reads
   for (int i=0; i<sizeof(buffer); ++i)
      std::cin.get(buffer[i]);
}
```

The [cppreference.com](http://cppreference.com/) and [cplusplus](http://www.cplusplus.com/) are good websites for all things to do with C++. The page on `std::cin` is [here](http://www.cplusplus.com/reference/iostream/cin/?kw=cin).
