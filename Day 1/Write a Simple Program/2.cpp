#include <iostream>

/**
 * mean - calculate mean of numbers
 * @arr: array of numbers
 * Return: mean
 */
int mean(int arr[])
{
  int sum = 0;
  int len = 0;

  while (arr[len] != '\0')
  {
    sum += arr[len];
    len++;
  }

  return (sum / len);
}

/**
 * main - entry point
 * Return: 0
 */
int main(void)
{
  int i = 0;
  int numbers[5];

  while (i < 5)
  {
    std::cout << "Please enter a number";
    std::cin >> numbers[i];
    i++;
  }

  int m = mean(numbers);

  std::cout << "first int " << numbers[0] << std::endl;

  std::cout << "The mean is " << m << std::endl;

  return (0);
}